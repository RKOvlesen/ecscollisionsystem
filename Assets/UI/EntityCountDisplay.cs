﻿using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

public class EntityCountDisplay : MonoBehaviour {

    public Text _uiText;

	private void Update ()
    {
        EntityManager em = World.Active.GetOrCreateManager<EntityManager>();
        var entities = em.GetAllEntities(Unity.Collections.Allocator.Temp);

        int entityCount = entities.Length;
        entities.Dispose();

        _uiText.text = "Entities: " + entityCount;
    }
}
