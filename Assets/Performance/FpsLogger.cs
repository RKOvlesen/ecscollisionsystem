﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

namespace Performance
{
    public class FpsLogger : MonoBehaviour
    {
        private const int Interval = 10;
        private FpsToText _fpsCounter;
        private DateTime lastUpdate;

        // Start is called before the first frame update
        private void Start()
        {
            _fpsCounter = GetComponent<FpsToText>();
            lastUpdate = DateTime.Now;
        }

        // Update is called once per frame
        private void Update()
        {
            DateTime now = DateTime.Now;
            TimeSpan diff = now - lastUpdate;
            if (diff.TotalSeconds > Interval)
            {
                lastUpdate = now;
                Analytics.CustomEvent("gameOver", new Dictionary<string, object>
                {
                    { "fps", _fpsCounter.Framerate }
                });
            }
        }
    }
}