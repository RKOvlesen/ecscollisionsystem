﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class GizmoDrawer : MonoBehaviour
{
    const int cellSize = 5;
    const float half = 0.5f;
    const int span = 1;

    [SerializeField]
    private bool DrawGrid = true;

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if(!DrawGrid)
            return;
        
        Gizmos.color = Color.blue;
        for (int x = -span; x <= span; x++)
        {
            for (int y = -span; y <= span; y++)
            {
                for (int z = -span; z <= span; z++)
                {
                    Vector3 center = new Vector3((cellSize * half) + x * cellSize, (cellSize * half) + y * cellSize, (cellSize * half) + z * cellSize);
                    Gizmos.DrawWireCube(center, Vector3.one * cellSize);
                }
            }
        }

        if (Selection.activeGameObject != null)
        {
            Gizmos.color = Color.green;

            Vector3 pos = Selection.activeGameObject.transform.position;// / cellSize;

            Vector3 center = new Vector3(Mathf.FloorToInt(pos.x / cellSize), Mathf.FloorToInt(pos.y / cellSize), Mathf.FloorToInt(pos.z / cellSize));
            Gizmos.DrawWireCube(center * cellSize + Vector3.one * (cellSize * half), Vector3.one * cellSize);
        }
    }
#endif
}
