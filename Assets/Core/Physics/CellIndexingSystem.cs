﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Core.Physics
{
    public class CellIndexingSystem : JobComponentSystem
    {
        private struct ColliderEntityGroup
        {
            [ReadOnly] public ComponentDataArray<Position> positions;
            public readonly int Length;
            public ComponentDataArray<EntityCollider> colliders;
        }

        [Inject] private ColliderEntityGroup m_ColliderEntityGroup;
        [Inject] private PhysicsSystem m_physicsSystem;

        [BurstCompile]
        private struct CellIndexingJob : IJobParallelFor
        {
            [ReadOnly] public ComponentDataArray<Position> readPositions;
            [ReadOnly] public float cellSize;
            public ComponentDataArray<EntityCollider> colliders;
            public NativeMultiHashMap<long, int>.Concurrent hashMap;

            public void Execute(int index)
            {
                //TODO Move static values like Size to a different component, so we don't have to read and write them all the time.
                float size = colliders[index].size;
                float3 position = readPositions[index].Value;

                //Find the cell coordinates and cell hash.
                int3 cellCoordinates = GridHash.Quantize(position, cellSize);
                long finalNodeIndex = GridHash.Hash(cellCoordinates);
                //Add to the hashmap(Essentially the 'octree')
                hashMap.Add(finalNodeIndex, index);

                //Calculate if the entity fits entirely in the cell. (Could this be simpler?)
                float3 sizeOffset = math.float3(1, 1, 1) * size * 0.5f;
                float3 cellSize3d = new float3(cellSize, cellSize, cellSize);
                float3 cellStart = cellCoordinates * cellSize3d;
                float3 cellEnd = cellStart + cellSize3d;
                bool3 startFits = (position - sizeOffset > cellStart);
                bool3 endFith = (position + sizeOffset >= cellEnd);

                bool fits = startFits.x && startFits.y && startFits.z && endFith.x && endFith.y && endFith.z; //I'm sure there is a smarter way to check bool3, but I can't find it

                colliders[index] = new EntityCollider
                {
                    size = size,
                    CellIndex = cellCoordinates,
                    CellHash = finalNodeIndex,
                    FullyContainedInCell = math.select(0, 1, fits)
                };
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            if (m_physicsSystem.CellIndexMap.IsCreated)
            {
                m_physicsSystem.CellIndexMap.Dispose();
            }

            int entityCount = m_ColliderEntityGroup.Length;

            m_physicsSystem.CellIndexMap = new NativeMultiHashMap<long, int>(entityCount, Allocator.TempJob);

            var cellIndexJob = new CellIndexingJob()
            {
                readPositions = m_ColliderEntityGroup.positions,
                colliders = m_ColliderEntityGroup.colliders,
                cellSize = Consts.CellSize,
                hashMap = m_physicsSystem.CellIndexMap.ToConcurrent()
            };
            var cellIndexHandle = cellIndexJob.Schedule(entityCount, 32, inputDeps);

            return cellIndexHandle;
        }

        protected override void OnDestroyManager()
        {
            if (m_physicsSystem.CellIndexMap.IsCreated)
            {
                m_physicsSystem.CellIndexMap.Dispose();
            }
        }

        private void PrintCellContent(int entityCount, float AreaSize)
        {
            string output = "------------\n";
            for (int i = 0; i < entityCount; i++)
            {
                float3 position = m_ColliderEntityGroup.positions[i].Value;
                long cellIndex = GridHash.Hash(position, AreaSize);
                int item;
                var iterator = new NativeMultiHashMapIterator<long>();

                output += "\nEntity: " + i + " in cell " + cellIndex + "\n";


                if (m_physicsSystem.CellIndexMap.TryGetFirstValue(cellIndex, out item, out iterator))
                {
                    output += "  " + item + "|, ";
                    while (m_physicsSystem.CellIndexMap.TryGetNextValue(out item, ref iterator))
                    {
                        output += item + ", ";
                    }
                }
                else
                {
                    output += "  -";
                }

            }

            Debug.Log(output);
        }
    }
}