﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
using UnityEngine;
using Core.Physics.Jobs;

namespace Core.Physics
{
    [UpdateAfter(typeof(CellIndexingSystem))]
    public class CollisionSystem : JobComponentSystem
    {
        struct ColliderEntityGroup
        {
            [ReadOnly] public EntityArray entities;
            [ReadOnly] public ComponentDataArray<Position> positions;
            public readonly int Length;
            public ComponentDataArray<EntityCollider> colliders;
            public ComponentDataArray<Velocity> velocities;
        }

        public struct CollisionPair
        {
            public Entity entityA;
            public Entity entityB;
        }

        [Inject] private ColliderEntityGroup m_ColliderEntityGroup;
        [Inject] private PhysicsSystem m_physicsSystem;

        [BurstCompile]
        struct CollissionCheckJob : IJobParallelFor
        {
            [ReadOnly] public EntityArray entities;
            [ReadOnly] public NativeArray<Position> positions;
            [ReadOnly] public NativeArray<Velocity> velocities;
            [ReadOnly] public NativeArray<EntityCollider> readColliders;
            [ReadOnly] public NativeMultiHashMap<long, int> hashMap;
            public NativeQueue<CollisionPair>.Concurrent collisionPairs;

            [ReadOnly] public float cellSize;
            [ReadOnly] public float deltaTime;

            public void Execute(int index)
            {
                EntityCollider collider = readColliders[index];
                float3 position = positions[index].Value;
                float size = collider.size;

                //Always do the simple check, for the cell that the entity exists in.
                DoSimpe(index, position, size, collider.CellHash);

                //Then, if the entity overlaps other cells, check them too.
                if (collider.FullyContainedInCell == 0)
                {
                    DoAdvanced(index, position, size, collider.CellIndex);
                }
            }

            /// <summary>
            /// Does collision check for the cell that the current entity is in. 
            /// </summary>
            private void DoSimpe(int index, float3 position, float size, long cellHash)
            {
                CollisionCalculator.CollisionCheck(index, cellHash, position, size, collisionPairs, hashMap, positions, velocities, readColliders, entities, deltaTime, true);
            }

            /// <summary>
            /// Does collision checks for all cells that the entity overlaps. (Except the entitys 'native' cell)
            /// </summary>
            private void DoAdvanced(int index, float3 position, float size, int3 cell)
            {
                //In order to have two large objects that span multiple cells hit each other, 
                //we have to check double entity size (full in each direction). This way, one of the two will reach the cell where the other entity exists.
                float3 sizeOffset = math.float3(1, 1, 1) * size;

                int3 start = GridHash.Quantize(position - sizeOffset, cellSize);
                int3 end = GridHash.Quantize(position + sizeOffset, cellSize);

                for (int x = start.x; x <= end.x; x++)
                {
                    for (int y = start.y; y <= end.y; y++)
                    {
                        for (int z = start.z; z <= end.z; z++)
                        {
                            int3 newCell = new int3(x, y, z);
                            //Don't check own cell!
                            bool3 sameCell = (newCell == cell);
                            if (sameCell.x && sameCell.y && sameCell.z) //Is there a simpler check for bool3?
                                continue;

                            long cellIndex = GridHash.Hash(newCell);
                            CollisionCalculator.CollisionCheck(index, cellIndex, position, size, collisionPairs, hashMap, positions, velocities, readColliders, entities, deltaTime, false);
                        }
                    }
                }
            }
        }

        [BurstCompile]
        struct ArrayToHashMapJob : IJobParallelFor
        {
            [ReadOnly] public NativeArray<CollisionPair> collisionPairs;
            public NativeMultiHashMap<Entity, Entity>.Concurrent pairMap;


            public void Execute(int index)
            {
                CollisionPair pair = collisionPairs[index];
                pairMap.Add(pair.entityA, pair.entityB);
            }
        }

        private NativeArray<EntityCollider> _readColliders;
        private NativeArray<Position> _readPositions; //This one is actually just an optimization. It's faster to read from than ComponentDataArray.
        private NativeArray<Velocity> _readVelocities;
        private NativeQueue<CollisionPair> _collisionResultQueue;

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            const float cellSize = Consts.CellSize;
            int entityCount = m_ColliderEntityGroup.Length;

            //TODO Is there a way to auto dispose after it has been used?
            if (_readColliders.IsCreated)
                _readColliders.Dispose();

            if (_readPositions.IsCreated)
                _readPositions.Dispose();

            if (_readVelocities.IsCreated)
                _readVelocities.Dispose();

            if (_collisionResultQueue.IsCreated)
                _collisionResultQueue.Clear();


            //m_ColliderEntityGroup size will vary, so we have to reallocate each frame.
            _readColliders = new NativeArray<EntityCollider>(entityCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
            _readPositions = new NativeArray<Position>(entityCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
            _readVelocities = new NativeArray<Velocity>(entityCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);

            var copyPositionDataJob = new CopyComponentData<Position>
            {
                Source = m_ColliderEntityGroup.positions,
                Results = _readPositions
            };
            var copyPositionsJobHandle = copyPositionDataJob.Schedule(entityCount, 16, inputDeps);
            
            var copyColliderDataJob = new CopyComponentData<EntityCollider>
            {
                Source = m_ColliderEntityGroup.colliders,
                Results = _readColliders
            };
            var copyColliderJobHandle = copyColliderDataJob.Schedule(entityCount, 16, inputDeps);

            var copyVelocityDataJob = new CopyComponentData<Velocity>
            {
                Source = m_ColliderEntityGroup.velocities,
                Results = _readVelocities
            };
            var copyVelocityJobHandle = copyVelocityDataJob.Schedule(entityCount, 16, inputDeps);

            var combinedCopyHandles = JobHandle.CombineDependencies(copyPositionsJobHandle, copyColliderJobHandle, copyVelocityJobHandle);

            var collisionCheckJob = new CollissionCheckJob()
            {
                entities = m_ColliderEntityGroup.entities,
                positions = _readPositions,
                readColliders = _readColliders,
                hashMap = m_physicsSystem.CellIndexMap,
                cellSize = cellSize,
                velocities = _readVelocities,
                deltaTime = Time.deltaTime,
                collisionPairs = _collisionResultQueue.ToConcurrent()
            };
            var collisionCheckHandle = collisionCheckJob.Schedule(entityCount, 64, combinedCopyHandles);
            collisionCheckHandle.Complete();


            // Dequeue pairs into array
            NativeArray<CollisionPair> collisionPairs = new NativeArray<CollisionPair>(_collisionResultQueue.Count, Allocator.TempJob);
            DequeueIntoArray<CollisionPair> dequeuePairsJob = new DequeueIntoArray<CollisionPair>()
            {
                InputQueue = _collisionResultQueue,
                OutputArray = collisionPairs
            };
            JobHandle dequeueCollisionPairs = dequeuePairsJob.Schedule(copyPositionsJobHandle);
            dequeueCollisionPairs.Complete();


            if (m_physicsSystem.CollisionResultMap.IsCreated)
            {
                m_physicsSystem.CollisionResultMap.Dispose();
            }

            m_physicsSystem.CollisionResultMap = new NativeMultiHashMap<Entity, Entity>(collisionPairs.Length, Allocator.TempJob);

            ArrayToHashMapJob arrayToHashMapJob = new ArrayToHashMapJob()
            {
                collisionPairs = collisionPairs,
                pairMap = m_physicsSystem.CollisionResultMap.ToConcurrent()
            };
            JobHandle arrayToHashMapHandle = arrayToHashMapJob.Schedule(collisionPairs.Length, 64, dequeueCollisionPairs);
            arrayToHashMapHandle.Complete();

            collisionPairs.Dispose();

            return arrayToHashMapHandle;
        }

        protected override void OnCreateManager()
        {
            _collisionResultQueue = new NativeQueue<CollisionPair>(Allocator.Persistent);
        }

        protected override void OnDestroyManager()
        {
            if (_readColliders.IsCreated)
                _readColliders.Dispose();

            if (_readPositions.IsCreated)
                _readPositions.Dispose();

            if (_readVelocities.IsCreated)
                _readVelocities.Dispose();

            if (_collisionResultQueue.IsCreated)
            {
                _collisionResultQueue.Dispose();
            }
        }
    }
}