﻿using Unity.Collections;
using Unity.Entities;

namespace Core.Physics
{
    public class PhysicsSystem : JobComponentSystem
    {
        public NativeMultiHashMap<Entity, Entity> CollisionResultMap;

        public NativeMultiHashMap<long, int> CellIndexMap;

        protected override void OnDestroyManager()
        {
            if (CollisionResultMap.IsCreated)
            {
                CollisionResultMap.Dispose();
            }

            if (CellIndexMap.IsCreated)
            {
                CellIndexMap.Dispose();
            }
        }
    }
}