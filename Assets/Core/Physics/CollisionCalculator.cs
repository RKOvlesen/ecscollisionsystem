﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using static Core.Physics.CollisionSystem;

namespace Core.Physics
{
    public static class CollisionCalculator
    {
        public static void CollisionCheck(int entityIndex, long cellHash, float3 position, float size, 
            NativeQueue<CollisionPair>.Concurrent resultQueue, NativeMultiHashMap<long, int> cellMap, 
            NativeArray<Position> positions, NativeArray<Velocity> velocities, 
            NativeArray<EntityCollider> readColliders, EntityArray entities, float deltaTime, bool checkOwnCell)
        {
            int otherEntityIndex;
            var iterator = new NativeMultiHashMapIterator<long>();
            float3 myVelocity = velocities[entityIndex].Value;

            if (cellMap.TryGetFirstValue(cellHash, out otherEntityIndex, out iterator))
            {
                //These lookups are the slowest operations, so it's much faster to create a variable once and pass it everywhere it's needed.
                float3 otherPosition = positions[otherEntityIndex].Value;
                float3 otherVelocity = velocities[otherEntityIndex].Value;
                EntityCollider otherCollider = readColliders[otherEntityIndex];

                //The NativeMultiHashMap is a little odd, as we get the first item at the same time as the iterator, so we have to process it separate from the iterated values.
                ProcessHit(entityIndex, otherEntityIndex, position, size, checkOwnCell, deltaTime, otherPosition, myVelocity, otherVelocity, otherCollider, resultQueue, entities);

                while (cellMap.TryGetNextValue(out otherEntityIndex, ref iterator))
                {
                    otherPosition = positions[otherEntityIndex].Value;
                    otherCollider = readColliders[otherEntityIndex];

                    ProcessHit(entityIndex, otherEntityIndex, position, size, checkOwnCell, deltaTime, otherPosition, myVelocity, otherVelocity, otherCollider, resultQueue, entities);
                }
            }
        }

        private static void ProcessHit(int entityIndex, int otherEntityIndex, float3 myPosition, 
            float itemSize, bool checkOwnCell, float deltaTime, float3 otherPosition, float3 myVelocity, float3 otherVelocity, 
            EntityCollider otherCollider, NativeQueue<CollisionPair>.Concurrent resultQueue, EntityArray entities)
        {
            bool hit = CalculateCollision(entityIndex, otherEntityIndex, myPosition, itemSize, otherPosition, otherCollider.size, myVelocity, otherVelocity);
            if (hit)
            {
                //When checking cells that the current entity is not itself in, we register the 'me -> you' hits too. 
                if (!checkOwnCell)
                {
                    if (otherCollider.FullyContainedInCell == 1)
                    {
                        resultQueue.Enqueue(new CollisionPair { entityA = entities[otherEntityIndex], entityB = entities[entityIndex] });
                    }
                    else
                    {
                        //But we have to make sure, that we don't add both pairs twice, if they hit each other.
                        const float halfSize = 0.5f;
                        float distance = math.lengthsq(myPosition - otherPosition);
                        float minDistance = (otherCollider.size + otherCollider.size) * halfSize;
                        bool otherOverlapsPosition = (distance <= minDistance * minDistance);
                        if (!otherOverlapsPosition)
                        {
                            resultQueue.Enqueue(new CollisionPair { entityA = entities[otherEntityIndex], entityB = entities[entityIndex] });
                        }
                    }
                }

                resultQueue.Enqueue(new CollisionPair { entityA = entities[entityIndex], entityB = entities[otherEntityIndex] });
            }
        }

        public static bool CalculateCollision(int entityIndex, int otherItemIndex, float3 myPosition, float itemSize, float3 otherPosition, float otherSize, float3 myVelocity, float3 otherVelocity)
        {
            if (entityIndex == otherItemIndex)
                return false;

            const float halfSize = 0.5f;

            //TODO Use Position, size and Velocity to calculate intersections between frames.

            float distance = math.lengthsq(myPosition - otherPosition);
            float minDistance = (itemSize + otherSize) * halfSize;
            return distance < minDistance * minDistance;
        }
    }
}