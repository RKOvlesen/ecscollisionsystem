﻿using System;
using Unity.Entities;
using Unity.Mathematics;

namespace Core.Physics
{
    [Serializable]
    public struct EntityCollider : IComponentData
    {
        public float size;
        public long CellHash;
        public int3 CellIndex;
        public int FullyContainedInCell;
    }

    public class EntityCollissionComponent : ComponentDataWrapper<EntityCollider> { }
}