﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Collections;
using UnityEngine;
using Unity.Burst;

namespace Core.Physics
{
    public class EntityVelocitySystem : JobComponentSystem
    {
        private const float Drag = 0.01f;

        [BurstCompile]
        struct VelocityCalculation : IJobProcessComponentData<Position, Velocity>
        {
            public float dt;
            public float drag;

            public void Execute(ref Position position, ref Velocity velocity)
            {
                position.Value = position.Value + velocity.Value * dt;
                velocity.Value -= velocity.Value * drag * dt;
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new VelocityCalculation() { dt = Time.deltaTime, drag = Drag };
            return job.Schedule(this, inputDeps);
        }
    }
}