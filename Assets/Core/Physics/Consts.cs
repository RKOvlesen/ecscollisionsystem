﻿namespace Core.Physics
{
    public static class Consts
    {
        public const float CellSize = 5;
        public const int RandomSeed = 154879324;
    }
}
