﻿using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;

namespace Core.Physics.Jobs
{
    [BurstCompile]
    public struct DequeueIntoArray<T> : IJob where T : struct
    {
        public int StartIndex;
        public NativeQueue<T> InputQueue;
        [WriteOnly] public NativeArray<T> OutputArray;

        public void Execute()
        {
            int queueCount = InputQueue.Count;

            for (int i = StartIndex; i < StartIndex + queueCount; i++)
            {
                OutputArray[i] = InputQueue.Dequeue();
            }
        }
    }
}