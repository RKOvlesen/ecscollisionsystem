﻿using Unity.Mathematics;

namespace Core.Physics
{
    public class GridHash
    {
        public readonly static int3[] cellOffsets =
        {
            new int3(0, 0, 0),
            new int3(-1, 0, 0),
            new int3(0, -1, 0),
            new int3(0, 0, -1),
            new int3(1, 0, 0),
            new int3(0, 1, 0),
            new int3(0, 0, 1)
        };

        public readonly static int2[] cell2DOffsets =
        {
            new int2(0, 0),
            new int2(-1, 0),
            new int2(0, -1),
            new int2(1, 0),
            new int2(0, 1),
        };

        public static long Hash(float3 v, float cellSize)
        {
            return Hash(Quantize(v, cellSize));
        }

        public static int3 Quantize(float3 v, float cellSize)
        {
            return new int3(math.floor(v / cellSize));
        }

        public static int Hash(float2 v, float cellSize)
        {
            return Hash(Quantize(v, cellSize));
        }

        public static int2 Quantize(float2 v, float cellSize)
        {
            return new int2(math.floor(v / cellSize));
        }

        public static long Hash(int3 grid)
        {
            unchecked
            {
                // Simple int3 hash based on a pseudo mix of :
                // 1) https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function
                // 2) https://en.wikipedia.org/wiki/Jenkins_hash_function
                /*int hash = grid.x;
                hash = (hash * 397) ^ grid.y;
                hash = (hash * 397) ^ grid.z;
                hash += hash << 3;
                hash ^= hash >> 11;
                hash += hash << 15;
                return hash;*/

                int firstResult = PerfectlyHashThem((short)grid.x, (short)grid.y);
                return PerfectlyHashThem(firstResult, grid.z);
            }
        }

        /// <summary>
        /// This will probably cause problems if the result of the 2 ints becomes larger than an int32.
        /// Unity.Mathematics does not have a math.aslong or math.asshort yet, so I will have to use int and just hope nothing goes wrong.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static long PerfectlyHashThem(int a, int b)
        {
            //var A = (ulong)(a >= 0 ? 2 * (long)a : -2 * (long)a - 1);
            //var B = (ulong)(b >= 0 ? 2 * (long)b : -2 * (long)b - 1);
            //var C = (long)((A >= B ? A * A + A + B : A + B * B) / 2);
            //return a < 0 && b < 0 || a >= 0 && b >= 0 ? C : -C - 1;

            var A = math.select(2 * a, -2 * a - 1, a >= 0);
            var B = math.select(2 * b, -2 * b - 1, b >= 0);
            var C = math.select(A * A + A + B, A + B * B, A >= B);
            bool positive = a < 0 && b < 0 || a >= 0 && b >= 0;
            return math.select(C, -C - 1, positive);
        }

        public static int PerfectlyHashThem(short a, short b)
        {
            // var A = math.asuint(a >= 0 ? 2 * a : -2 * a - 1);
            //var B = math.asuint(b >= 0 ? 2 * b : -2 * b - 1);
            //var C = math.asint((A >= B ? A * A + A + B : A + B * B) / 2);
            //return a < 0 && b < 0 || a >= 0 && b >= 0 ? C : -C - 1;

            var A = math.select(2 * a, -2 * a - 1, a >= 0);
            var B = math.select(2 * b, -2 * b - 1, b >= 0);
            var C = math.select(A * A + A + B, A + B * B, A >= B);
            bool positive = a < 0 && b < 0 || a >= 0 && b >= 0;
            return math.select(C, -C - 1, positive);
        }

        public static int Hash(int2 grid)
        {
            unchecked
            {
                // Simple int3 hash based on a pseudo mix of :
                // 1) https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function
                // 2) https://en.wikipedia.org/wiki/Jenkins_hash_function
                int hash = grid.x;
                hash = (hash * 397) ^ grid.y;
                hash += hash << 3;
                hash ^= hash >> 11;
                hash += hash << 15;
                return hash;
            }
        }

        public static ulong Hash(ulong hash, ulong key)
        {
            const ulong m = 0xc6a4a7935bd1e995UL;
            const int r = 47;

            ulong h = hash;
            ulong k = key;

            k *= m;
            k ^= k >> r;
            k *= m;

            h ^= k;
            h *= m;

            h ^= h >> r;
            h *= m;
            h ^= h >> r;

            return h;
        }
    }
}
