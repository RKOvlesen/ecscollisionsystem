﻿using Unity.Mathematics;
using UnityEngine;

namespace Core.Physics
{
    public class CollisionPhysics
    {
        /// <summary>
        /// Magic number from the code this was taken from.
        /// Higher value gives more powerful bounce.
        /// </summary>
        private const float Restitution = 0.85f; 

        /// <summary>
        /// Calculations borrowed from https://stackoverflow.com/questions/345838/ball-to-ball-collision-detection-and-handling
        /// </summary>
        /// <returns></returns>
        public static float3 CalculateCollisionPhysics(float3 itemPosition, float itemSize, float3 otherPosition, float otherSize, float3 itemVelocity, float3 otherVelocity, float3 randomFloat)
        {
            // get the mtd
            float3 delta = itemPosition - otherPosition;
            float d = math.length(delta);
            if (d < 0.0001f)
            {
                return randomFloat;
            }

            // minimum translation distance to push balls apart after intersecting
            //Vector2d mtd = delta.multiply(((getRadius() + ball.getRadius()) - d) / d);
            float3 mtd = delta * ((((itemSize / 2) + (otherSize / 2)) - d) / d);

            // resolve intersection --
            // inverse mass quantities
            float im1 = 1 / itemSize; //Mass
            float im2 = 1 / otherSize; //Mass

            // push-pull them apart based off their mass
            //position = position.add(mtd.multiply(im1 / (im1 + im2)));
            //ball.position = ball.position.subtract(mtd.multiply(im2 / (im1 + im2)));

            // impact speed
            float3 v = (itemVelocity - otherVelocity);
            float vn = math.dot(v, math.normalize(mtd));

            // sphere intersecting but moving away from each other already
            if (vn > 0.0f)
                return new float3();
            else if (vn == 0.0f)
                return mtd * (im1 / (im1 + im2));

            // collision impulse
            float i = (-(1.0f + Restitution) * vn) / (im1 + im2);
            float3 impulse = mtd * i;

            // change in momentum
            return impulse * im1;
            //this.velocity = this.velocity.add(impulse.multiply(im1));
            //ball.velocity = ball.velocity.subtract(impulse.multiply(im2));
        }
    }
}
