﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Collections;
using UnityEngine;
using Unity.Mathematics;
using Unity.Burst;

namespace Samples.Common
{
    public class EntityRotationSystem : JobComponentSystem
    {
        private const float Drag = 3.0f;

        [BurstCompile]
        struct VelocityCalculation : IJobProcessComponentData<Rotation, RotationalVelocity>
        {
            public float dt;
            public float drag;

            public void Execute(ref Rotation rotation, ref RotationalVelocity velocity)
            {
                //math.euler is not implemented yet... So we do it manually
                //rotation.Value = math.mul(rotation.Value, math.euler(velocity.Value));

                quaternion original = rotation.Value;
                float3 up = math.up();
                rotation.Value = math.mul(math.normalize(rotation.Value), quaternion.AxisAngle(up, velocity.Value.y * dt));
                float3 right = new float3(1, 0, 0);
                rotation.Value = math.mul(math.normalize(rotation.Value), quaternion.AxisAngle(right, velocity.Value.x * dt));
                float3 forward = new float3(0, 0, 1);
                rotation.Value = math.mul(math.normalize(rotation.Value), quaternion.AxisAngle(forward, velocity.Value.z * dt));

                velocity.Value -= velocity.Value * drag * dt;
            }

            private float3 GetRight(quaternion rotation)
            {
                return math.cross(math.forward(rotation), math.up());
            }

            private float3 GetUp(quaternion rotation)
            {
                float3 right = GetRight(rotation);
                return math.cross(math.forward(rotation), right);
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new VelocityCalculation() { dt = Time.deltaTime, drag = Drag };
            return job.Schedule(this, inputDeps);
        }
    }
}