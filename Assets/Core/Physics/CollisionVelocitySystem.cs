﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

namespace Core.Physics
{
    [UpdateAfter(typeof(CollisionSystem))]
    public class CollisionVelocitySystem : JobComponentSystem
    {
        //TODO Use ArchTypes instead
        struct ColliderEntityGroup
        {
            [ReadOnly] public EntityArray entities;

            //These are never actually used, but they act as filters so we only get Entities with these components on.
            public ComponentDataArray<Velocity> velocities;
            public ComponentDataArray<Position> positions;
            public ComponentDataArray<EntityCollider> colliders;
            public readonly int Length;
        }

        //TODO Use ArchTypes instead
        [Inject] public ComponentDataFromEntity<Position> Positions;
        [Inject] public ComponentDataFromEntity<EntityCollider> Colliders;
        [Inject] public ComponentDataFromEntity<Velocity> Velocities;

        [Inject] private ColliderEntityGroup m_ColliderEntityGroup;

        [Inject] private PhysicsSystem m_physicsSystem;

        private Random _random;

        protected override void OnCreateManager()
        {
            _random = new Random(Consts.RandomSeed);
        }

        [BurstCompile]
        struct VelocityCalculationJob : IJobParallelFor
        {
            [ReadOnly] public EntityArray entities;
            [ReadOnly] public ComponentDataFromEntity<Position> positions;
            [ReadOnly] public ComponentDataFromEntity<EntityCollider> readColliders;
            [ReadOnly] public NativeMultiHashMap<Entity, Entity> collisionResults;
            [ReadOnly] public ComponentDataFromEntity<Velocity> velocities;
            public Random random;
            public NativeArray<Velocity> velocityResults;

            public void Execute(int index)
            {
                NativeMultiHashMapIterator<Entity> iterator;
                Entity thisEntity = entities[index];
                Entity otherEntityIndex;
                if (collisionResults.TryGetFirstValue(thisEntity, out otherEntityIndex, out iterator))
                {
                    EntityCollider collider = readColliders[thisEntity];
                    float3 position = positions[thisEntity].Value;
                    float3 velocity = velocities[thisEntity].Value;
                    float size = collider.size;

                    int collisionCount = 0;
                    float3 otherPosition = positions[otherEntityIndex].Value;
                    float otherSize = readColliders[otherEntityIndex].size;
                    float3 otherVelocity = velocities[otherEntityIndex].Value;
                    

                    float3 velocityAddition = CollisionPhysics.CalculateCollisionPhysics(position, size, otherPosition, otherSize, velocity, otherVelocity, random.NextFloat3());
                    collisionCount++;

                    while (collisionResults.TryGetNextValue(out otherEntityIndex, ref iterator))
                    {
                        collisionCount++;
                        otherPosition = positions[otherEntityIndex].Value;
                        otherSize = readColliders[otherEntityIndex].size;
                        otherVelocity = velocities[otherEntityIndex].Value;
                        velocityAddition += CollisionPhysics.CalculateCollisionPhysics(position, size, otherPosition, otherSize, velocity, otherVelocity, random.NextFloat3());
                    }

                    velocityResults[index] = new Velocity
                    {
                        Value = velocityAddition,
                        Collides = collisionCount
                    };
                }
            }
        }

        [BurstCompile]
        struct ApplyVelocityJob : IJobParallelFor
        {
            [ReadOnly] public EntityArray entities;
            [ReadOnly] public NativeArray<Velocity> velocityResults;
            [NativeDisableParallelForRestriction] public ComponentDataFromEntity<Velocity> velocities;

            public void Execute(int index)
            {
                Entity thisEntity = entities[index];
                Velocity result = velocityResults[index];
                velocities[thisEntity] = new Velocity()
                {
                    Value = velocities[thisEntity].Value + result.Value,
                    Collides = result.Collides
                };
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            //Save the list of entities that are keys in the CollisionResultMap, and iterate over them instead.
            int entityCount = m_ColliderEntityGroup.Length;

            NativeArray<Velocity> velocityResults = new NativeArray<Velocity>(entityCount, Allocator.TempJob);

            VelocityCalculationJob velocityJob = new VelocityCalculationJob()
            {
                entities = m_ColliderEntityGroup.entities,
                positions = Positions,
                readColliders = Colliders,
                collisionResults = m_physicsSystem.CollisionResultMap,
                velocities = Velocities,
                random = _random,
                velocityResults = velocityResults
            };

            var calculationJobHandle = velocityJob.Schedule(entityCount, 32, inputDeps);
            calculationJobHandle.Complete();

            ApplyVelocityJob applyVelocityJob = new ApplyVelocityJob()
            {
                entities = m_ColliderEntityGroup.entities,
                velocities = Velocities,
                velocityResults = velocityResults
            };

            var applyJobHandle = applyVelocityJob.Schedule(entityCount, 32, calculationJobHandle);

            applyJobHandle.Complete();

            velocityResults.Dispose();

            return applyJobHandle;
        }
    }
}
