﻿using System;
using Unity.Entities;
using Unity.Mathematics;

[Serializable]
public struct RotationalVelocity : IComponentData
{
    public float3 Value;
}

public class RotationalVelocityComponent : ComponentDataWrapper<RotationalVelocity> { }