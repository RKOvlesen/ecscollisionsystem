﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Samples.Common
{
    public class SpawnRandomCubeSystem : ComponentSystem
    {
        struct Group
        {
            [ReadOnly] public SharedComponentDataArray<SpawnRandomCube> Spawner;
            public readonly int Length;
            public ComponentDataArray<Position> Position;
            public EntityArray Entity;
        }

        [Inject] Group m_Group;

        protected override void OnUpdate()
        {
            if (m_Group.Length == 0)
                return;

            SpawnRandomCube spawner = m_Group.Spawner[0];
            Entity sourceEntity = m_Group.Entity[0];
            float3 center = m_Group.Position[0].Value;

            var entities = new NativeArray<Entity>(spawner.count, Allocator.Temp);
            EntityManager.Instantiate(spawner.prefab, entities);

            for (int i = 0; i < spawner.count; i++)
            {
                Vector3 randomPos = UnityEngine.Random.insideUnitSphere * spawner.radius;

                var position = new Position
                {
                    Value = center + ToFloat3(randomPos)
                    //Value = new float3(0, 0, 0)
                };

                EntityManager.SetComponentData(entities[i], position);

                /*var velocity = new Velocity
                {
                    Value = Random.insideUnitSphere * Random.Range(5, 50)
                };

                EntityManager.SetComponentData(entities[i], velocity);

                var rotaionalVelocity = new RotationalVelocity
                {
                    Value = ToFloat3(Random.insideUnitSphere * 2)
                };

                EntityManager.SetComponentData(entities[i], rotaionalVelocity);*/
            }

            entities.Dispose();

            EntityManager.RemoveComponent<SpawnRandomCube>(sourceEntity);

            // Instantiate & AddComponent & RemoveComponent calls invalidate the injected groups,
            // so before we get to the next spawner we have to reinject them  
            UpdateInjectedComponentGroups();
        }

        private float3 ToFloat3(Vector3 vector)
        {
            return new float3(vector.x, vector.y, vector.z);
        }
    }
}
