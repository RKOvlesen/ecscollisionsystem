﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Game
{
    public class PlayerMovementSystem : JobComponentSystem
    {
        struct PlayerEntityGroup
        {
            public readonly int Length;
            [ReadOnly] public ComponentDataArray<PlayerControl> playerControllers;
            [ReadOnly] public ComponentDataArray<Rotation> rotations;
            public ComponentDataArray<RotationalVelocity> rotationalVelocities;
            public ComponentDataArray<Velocity> velocities;
        }

        [Inject] private PlayerEntityGroup m_PlayerEntityGroup;


        [BurstCompile]
        struct VelocityControlJob : IJobParallelFor
        {
            [ReadOnly] public int3 velocityDirection;
            [ReadOnly] public float thrustPower;
            [ReadOnly] public float3 mouseRotation;
            [ReadOnly] public float rotationPower;
            [ReadOnly] public float deltaTime;
            [ReadOnly] public ComponentDataArray<Rotation> rotations;

            public ComponentDataArray<RotationalVelocity> rotationalVelocities;
            public ComponentDataArray<Velocity> velocities;

            public void Execute(int index)
            {
                float thrust = thrustPower * deltaTime;

                float3 directionalThrust = new float3(velocityDirection.x * thrust, velocityDirection.y * thrust, velocityDirection.z * thrust);

                float3 currentVelocity = velocities[index].Value;

                float3 velocityAdd = math.mul(rotations[index].Value, directionalThrust);

                velocities[index] = new Velocity()
                {
                    Value = currentVelocity + velocityAdd
                };

                float3 originalRotation = rotationalVelocities[index].Value;

                rotationalVelocities[index] = new RotationalVelocity()
                {
                    Value = originalRotation += mouseRotation * rotationPower
                };
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            const float Speed = 5;
            const float RotationSpeed = 2;

            int3 velocityDirection = GetXYZInput();
            float3 mouseRotation = GetMouseMovement();

            var velocityControlJob = new VelocityControlJob()
            {
                velocityDirection = velocityDirection,
                thrustPower = Speed,
                mouseRotation = mouseRotation,
                rotationPower = RotationSpeed,
                deltaTime = Time.deltaTime,
                rotations = m_PlayerEntityGroup.rotations,
                rotationalVelocities = m_PlayerEntityGroup.rotationalVelocities,
                velocities = m_PlayerEntityGroup.velocities
            };

            return velocityControlJob.Schedule(m_PlayerEntityGroup.Length, 4, inputDeps);
        }

        private int3 GetXYZInput()
        {
            int3 velocityDirection = new int3(0, 0, 0);

            if (Input.GetKey(KeyCode.W))
            {
                velocityDirection.z += 1;
            }

            if (Input.GetKey(KeyCode.S))
            {
                velocityDirection.z -= 1;
            }


            if (Input.GetKey(KeyCode.D))
            {
                velocityDirection.x += 1;
            }

            if (Input.GetKey(KeyCode.A))
            {
                velocityDirection.x -= 1;
            }
            

            if (Input.GetKey(KeyCode.Space))
            {
                velocityDirection.y += 1;
            }

            if (Input.GetKey(KeyCode.C) || Input.GetKey(KeyCode.LeftControl))
            {
                velocityDirection.y -= 1;
            }

            return velocityDirection;
        }

        private float3 GetMouseMovement()
        {
            float rotationX = 0;
            float rotationY = 0;
            float rotationZ = 0;

            if (Input.GetKey(KeyCode.Q))
                rotationZ += 1;

            if (Input.GetKey(KeyCode.E))
                rotationZ -= 1;

            if (Input.GetMouseButton(1))
            {
                float width = (Screen.width * 0.5f);
                float height = (Screen.height * 0.5f);

                float mouseXOffset = (width - Input.mousePosition.x) / width;
                float mouseYOffset = (height - Input.mousePosition.y) / height;

                //We rotate around an axis, so the Mouse X controls Y rotation. (And Y is upside down, so * -1)
                rotationY = mouseXOffset * -1;
                rotationX = mouseYOffset;
            }
            else
            {
                if (Input.GetKey(KeyCode.LeftArrow))
                    rotationY -= 1;

                if (Input.GetKey(KeyCode.RightArrow))
                    rotationY += 1;
            }

            return new float3(rotationX, rotationY, rotationZ) * Time.deltaTime;
        }
    }
}