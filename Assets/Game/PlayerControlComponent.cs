﻿using System;
using Unity.Entities;

namespace Game
{
    [Serializable]
    public struct PlayerControl : IComponentData
    {

    }

    public class PlayerControlComponent : ComponentDataWrapper<PlayerControl> { }
}