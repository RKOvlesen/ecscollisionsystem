﻿using Core.Physics;
using NUnit.Framework;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Tests
{
    public class ECSTestFixture
    {
        public const int DefaultEntitySize = 1;

        protected World m_PreviousWorld;
        protected World World;
        protected EntityManager m_Manager;

        [SetUp]
        public virtual void Setup()
        {
            m_PreviousWorld = World.Active;
            World = World.Active = new World("Test World");

            m_Manager = World.GetOrCreateManager<EntityManager>();
        }

        [TearDown]
        public virtual void TearDown()
        {
            if (m_Manager != null)
            {
                World.Dispose();
                World = null;

                World.Active = m_PreviousWorld;
                m_PreviousWorld = null;
                m_Manager = null;
            }
        }

        public Entity CreateEntityWithDefaultData(float3 position, int size = DefaultEntitySize, float3 velocity = new float3())
        {
            var entity = m_Manager.CreateEntity(typeof(EntityCollider), typeof(Position), typeof(Velocity));

            // Setup some non zero default values
            m_Manager.SetComponentData(entity, new EntityCollider { size = size });
            m_Manager.SetComponentData(entity, new Position { Value = position });
            m_Manager.SetComponentData(entity, new Velocity { Value = velocity });

            return entity;
        }

        public void UpdateWorld()
        {
            World.GetOrCreateManager<CellIndexingSystem>().Update();
            World.GetOrCreateManager<CollisionSystem>().Update();
            World.GetOrCreateManager<CollisionVelocitySystem>().Update();
        }

        protected void AssertFloat3NotNan(float3 value)
        {
            Assert.IsFalse(float.IsNaN(value.x), "x value is NaNf");
            Assert.IsFalse(float.IsNaN(value.y), "y value is NaNf");
            Assert.IsFalse(float.IsNaN(value.z), "z value is NaNf");
        }
    }
}