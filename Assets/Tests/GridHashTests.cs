﻿using NUnit.Framework;
using Unity.Mathematics;
using Core.Physics;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Tests
{
    public class GridHashTests
    {
        [Test]
        public void GridHash_000_Is1()
        {
            long hash = GridHash.Hash(new float3(0, 0, 0), 1);
            Assert.AreEqual(1, hash);

            long hash1 = GridHash.Hash(new float3(0, 0, 0), 10);
            Assert.AreEqual(1, hash1);

            long hash2 = GridHash.Hash(new float3(0, 0, 0), Consts.CellSize);
            Assert.AreEqual(1, hash2);
        }

        [Test]
        public void GridHash_TestMinus10To10_AllUnique()
        {
            int3 start = new int3(-10, -10, -10);
            int3 end = new int3(10, 10, 10);
            List<long> hashes = new List<long>();

            for (int x = start.x; x <= end.x; x++)
            {
                for (int y = start.y; y <= end.y; y++)
                {
                    for (int z = start.z; z <= end.z; z++)
                    {
                        int3 cellIndex = new int3(x, y, z);
                        hashes.Add(GridHash.Hash(cellIndex));
                    }
                }
            }

            int uniqueIds = hashes.Distinct().Count();
            Assert.AreEqual(9261, uniqueIds);
        }
    }
}