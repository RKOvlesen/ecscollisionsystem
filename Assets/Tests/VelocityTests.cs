﻿using Core.Physics;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class VelocityTests : ECSTestFixture
    {
        [UnityTest]
        public IEnumerator SingleEntity_AtStart_NoVelocity()
        {
            Entity entity = CreateEntityWithDefaultData(new float3(0, 0, 0));

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            Velocity velocity = m_Manager.GetComponentData<Velocity>(entity);

            Assert.AreEqual(0, velocity.Value.x);
            Assert.AreEqual(0, velocity.Value.y);
            Assert.AreEqual(0, velocity.Value.z);
        }

        [UnityTest]
        public IEnumerator TwoEntities_NotTouching_NoVelocity()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0));
            Entity entity2 = CreateEntityWithDefaultData(new float3(10, 0, 0));

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            Velocity velocity1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity velocity2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(0, velocity1.Value.x);
            Assert.AreEqual(0, velocity1.Value.y);
            Assert.AreEqual(0, velocity1.Value.z);

            Assert.AreEqual(0, velocity2.Value.x);
            Assert.AreEqual(0, velocity2.Value.y);
            Assert.AreEqual(0, velocity2.Value.z);
        }

        [UnityTest]
        public IEnumerator ManyEntities_NotTouching_NoVelocity()
        {
            const int EntityCount = 10;
            List<Entity> entities = new List<Entity>();

            for (int i = 0; i < EntityCount; i++)
            {
                Entity entity = CreateEntityWithDefaultData(new float3(DefaultEntitySize * i * 2, 0, 0));
                entities.Add(entity);
            }

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            Assert.AreEqual(entities.Count, EntityCount);

            for (int i = 0; i < EntityCount; i++)
            {
                Velocity velocity = m_Manager.GetComponentData<Velocity>(entities[i]);
                Assert.AreEqual(0, velocity.Value.x);
                Assert.AreEqual(0, velocity.Value.y);
                Assert.AreEqual(0, velocity.Value.z);
            }
        }

        [UnityTest]
        public IEnumerator TwoEntities_SamePosition_HasVelocity()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0));
            Entity entity2 = CreateEntityWithDefaultData(new float3(0, 0, 0));

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            Velocity velocity1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity velocity2 = m_Manager.GetComponentData<Velocity>(entity2);

            AssertFloat3NotNan(velocity1.Value);
            float totalVelocity1 = math.abs(velocity1.Value.x + velocity1.Value.y + velocity1.Value.z);
            Assert.Greater(totalVelocity1, 0);

            AssertFloat3NotNan(velocity2.Value);
            float totalVelocity2 = math.abs(velocity2.Value.x + velocity2.Value.y + velocity2.Value.z);
            Assert.Greater(totalVelocity2, 0);

            Assert.AreNotEqual(totalVelocity1, totalVelocity2);
        }

        [UnityTest]
        public IEnumerator ManyEntities_SamePosition_HasVelocity()
        {
            const int EntityCount = 10;
            List<Entity> entities = new List<Entity>();

            for (int i = 0; i < EntityCount; i++)
            {
                Entity entity = CreateEntityWithDefaultData(new float3(0, 0, 0));
                entities.Add(entity);
            }

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            Assert.AreEqual(entities.Count, EntityCount);

            for (int i = 0; i < EntityCount; i++)
            {
                Velocity velocity = m_Manager.GetComponentData<Velocity>(entities[i]);
                float totalVelocity = math.abs(velocity.Value.x + velocity.Value.y + velocity.Value.z);
                Assert.Greater(totalVelocity, 0);
            }
        }

        [UnityTest]
        public IEnumerator TwoEntities_VeryClose_HasVelocity()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, -0.05f, 0));
            Entity entity2 = CreateEntityWithDefaultData(new float3(0.01f, 0, 0.002f));

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            Velocity velocity1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity velocity2 = m_Manager.GetComponentData<Velocity>(entity2);
            
            AssertFloat3NotNan(velocity1.Value);
            float totalVelocity1 = math.abs(velocity1.Value.x + velocity1.Value.y + velocity1.Value.z);
            Assert.Greater(totalVelocity1, 0);

            AssertFloat3NotNan(velocity2.Value);
            float totalVelocity2 = math.abs(velocity1.Value.x + velocity1.Value.y + velocity1.Value.z);
            Assert.Greater(totalVelocity2, 0);
        }

        [UnityTest]
        public IEnumerator ManyEntities_VeryClose_HasVelocity()
        {
            const int EntityCount = 5; //Because they are so close, they will all collide and cause too many entires in the hashmap.
            List<Entity> entities = new List<Entity>();

            entities.Add(CreateEntityWithDefaultData(new float3(0, -0.05f, 0)));
            entities.Add(CreateEntityWithDefaultData(new float3(0.01f, 0, 0.002f)));
            entities.Add(CreateEntityWithDefaultData(new float3(-0.01f, 0.11f, 0.002f)));
            entities.Add(CreateEntityWithDefaultData(new float3(0.12f, 0, -0.007f)));
            entities.Add(CreateEntityWithDefaultData(new float3(0.0f, 0.0247f, -0.01f)));

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            Assert.AreEqual(entities.Count, EntityCount);

            for (int i = 0; i < EntityCount; i++)
            {
                Velocity velocity = m_Manager.GetComponentData<Velocity>(entities[i]);
                float totalVelocity = math.abs(velocity.Value.x + velocity.Value.y + velocity.Value.z);
                Assert.Greater(totalVelocity, 0);
            }
        }

        [UnityTest]
        public IEnumerator TwoEntities_BarelyTouch_HasVelocity()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0));
            Entity entity2 = CreateEntityWithDefaultData(new float3(DefaultEntitySize * 0.99f, 0, 0));

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            Velocity velocity1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity velocity2 = m_Manager.GetComponentData<Velocity>(entity2);

            AssertFloat3NotNan(velocity1.Value);
            float totalVelocity1 = math.abs(velocity1.Value.x + velocity1.Value.y + velocity1.Value.z);
            Assert.Greater(totalVelocity1, 0);

            AssertFloat3NotNan(velocity2.Value);
            float totalVelocity2 = math.abs(velocity1.Value.x + velocity1.Value.y + velocity1.Value.z);
            Assert.Greater(totalVelocity2, 0);
        }

        [UnityTest]
        public IEnumerator TwoDifferentSizeEntities_BarelyTouch_HasVelocity()
        {
            int largeEntitySize = 10;
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0), DefaultEntitySize);
            Entity entity2 = CreateEntityWithDefaultData(new float3((largeEntitySize + DefaultEntitySize) * 0.45f, 0, 0), largeEntitySize);

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            Velocity velocity1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity velocity2 = m_Manager.GetComponentData<Velocity>(entity2);

            AssertFloat3NotNan(velocity1.Value);
            float totalVelocity1 = math.abs(velocity1.Value.x + velocity1.Value.y + velocity1.Value.z);
            Assert.Greater(totalVelocity1, 0);

            AssertFloat3NotNan(velocity2.Value);
            float totalVelocity2 = math.abs(velocity1.Value.x + velocity1.Value.y + velocity1.Value.z);
            Assert.Greater(totalVelocity2, 0);
        }

        [UnityTest]
        public IEnumerator ManyEntities_BarelyTouch_HasVelocity()
        {
            const int EntityCount = 5; //Because they are so close, they will all collide and cause too many entires in the hashmap.
            List<Entity> entities = new List<Entity>();

            entities.Add(CreateEntityWithDefaultData(new float3(0, -DefaultEntitySize * 0.49f, 0)));
            entities.Add(CreateEntityWithDefaultData(new float3(-DefaultEntitySize * 0.49f, 0, 0)));
            entities.Add(CreateEntityWithDefaultData(new float3(0, DefaultEntitySize * 0.49f, 0)));
            entities.Add(CreateEntityWithDefaultData(new float3(-DefaultEntitySize * 0.49f, 0, -DefaultEntitySize * 0.49f)));
            entities.Add(CreateEntityWithDefaultData(new float3(DefaultEntitySize * 0.49f, 0, DefaultEntitySize * 0.49f)));

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            Assert.AreEqual(entities.Count, EntityCount);

            for (int i = 0; i < EntityCount; i++)
            {
                Velocity velocity = m_Manager.GetComponentData<Velocity>(entities[i]);
                AssertFloat3NotNan(velocity.Value);
                float totalVelocity1 = math.abs(velocity.Value.x + velocity.Value.y + velocity.Value.z);
                Assert.Greater(totalVelocity1, 0, "Entity "+i+" velocity not as expected! "+velocity.Value);
            }
        }

        [UnityTest]
        public IEnumerator SingleEntity_HasVelocity_MaintainsVelocity()
        {
            float xMovement = -10;

            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0), DefaultEntitySize, new float3(xMovement, 0, 0));

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            Velocity velocity = m_Manager.GetComponentData<Velocity>(entity1);
            Assert.Less(Mathf.Abs(xMovement - velocity.Value.x), 0.0000001f, "Assuming no drag, the velocities should be equal ("+xMovement+" == "+velocity.Value.x+")");
        }
    }
}