﻿using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Unity.Mathematics;
using Core.Physics;
using Unity.Entities;
using UnityEngine;
using System.Collections.Generic;

namespace Tests
{
    public class CollisionDetectionSizeTests : ECSTestFixture
    {
        [Test]
        public void CreateEntity_Default_HasSize1()
        {
            Entity entity = CreateEntityWithDefaultData(new float3(0, 0, 0));
            EntityCollider collider = m_Manager.GetComponentData<EntityCollider>(entity);
            Velocity velocityComp = m_Manager.GetComponentData<Velocity>(entity);
            Assert.AreEqual(1, collider.size);
            Assert.AreEqual(0, velocityComp.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_SingleLargeEntity_NoCollisions()
        {
            Entity entity = CreateEntityWithDefaultData(new float3(0, 0, 0), 100);

            yield return new WaitForEndOfFrame();

            Velocity collider = m_Manager.GetComponentData<Velocity>(entity);

            Assert.AreEqual(0, collider.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoLargeEntities_SamePositions_Collide()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0), 100);
            Entity entity2 = CreateEntityWithDefaultData(new float3(0, 0, 0), 100);

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoLargeEntities_SamePositions_Extreme_Collide()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(10000, -20000, 7500), 100);
            Entity entity2 = CreateEntityWithDefaultData(new float3(10000, -20000, 7500), 100);

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoLargeEntities_OverlapCellsClose_Collide()
        {
            int size = 50;
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0), size);
            Entity entity2 = CreateEntityWithDefaultData(new float3(Consts.CellSize * 1.1f, 0, 0), size);

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoLargeEntities_OverlapCellsFar_Collide()
        {
            int size = 50;
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0), size);
            Entity entity2 = CreateEntityWithDefaultData(new float3(size * 0.45f, 0, 0), size);

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoLargeEntities_BarelyTouch_Collide()
        {
            int size = 25;
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0), size);
            Entity entity2 = CreateEntityWithDefaultData(new float3(size * 0.99f, 0, 0), size);

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoDifferentLargeEntities_NoTouch_DontCollide()
        {
            int size1 = 25;
            int size2 = 100;
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0), size1);
            Entity entity2 = CreateEntityWithDefaultData(new float3(size1 + size2, 0, 0), size2);

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(0, collider1.Collides);
            Assert.AreEqual(0, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoDifferentLargeEntities_BarelyTouch_Collide()
        {
            int size1 = 25;
            int size2 = 100;
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0), size1);
            Entity entity2 = CreateEntityWithDefaultData(new float3((size1 + size2) * 0.49f, 0, 0), size2);

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoDifferentLargeEntities_MaxTouchDistance_Collide()
        {
            int size1 = 25;
            int size2 = 100;
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0), size1);
            Entity entity2 = CreateEntityWithDefaultData(new float3(1, 1, 1) * size2 * -0.36f, size2);

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoDifferentLargeEntities_Inverted_MaxTouchDistance_Collide()
        {
            int size1 = 25;
            int size2 = 100;
            Entity entity1 = CreateEntityWithDefaultData(new float3(1, 1, 1) * size2 * -0.36f, size1);
            Entity entity2 = CreateEntityWithDefaultData(new float3(0, 0, 0), size2);

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }
    }
}