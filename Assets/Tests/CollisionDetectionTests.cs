﻿using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Unity.Mathematics;
using Core.Physics;
using Unity.Entities;
using UnityEngine;
using System.Collections.Generic;

namespace Tests
{
    public class CollisionDetectionTests : ECSTestFixture
    {
        [UnityTest]
        public IEnumerator Collision_SingleEntity_NoCollisions()
        {
            Entity entity = CreateEntityWithDefaultData(new float3(0, 0, 0));

            yield return new WaitForEndOfFrame();

            Velocity velocityComp = m_Manager.GetComponentData<Velocity>(entity);

            Assert.AreEqual(0, velocityComp.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoEntities_SamePositions_Collide()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0));
            Entity entity2 = CreateEntityWithDefaultData(new float3(0, 0, 0));

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoEntities_SamePositions_Extreme_Collide()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(10000, -20000, 7500));
            Entity entity2 = CreateEntityWithDefaultData(new float3(10000, -20000, 7500));

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoEntities_BarelyTouch_Collide()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0));
            Entity entity2 = CreateEntityWithDefaultData(new float3(DefaultEntitySize * 0.99f, 0, 0));

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoEntities_BarelyTouch_Extreme_Collide()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(-50000, 50000, -50000));
            Entity entity2 = CreateEntityWithDefaultData(new float3(-50000 + DefaultEntitySize * 0.99f, 50000, -50000));

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_XYZEntities_BarelyTouch_Collide()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0));
            Entity entityx = CreateEntityWithDefaultData(new float3(DefaultEntitySize * 0.99f, 0, 0));
            Entity entityy = CreateEntityWithDefaultData(new float3(0, DefaultEntitySize * 0.99f, 0));
            Entity entityz = CreateEntityWithDefaultData(new float3(0, 0, DefaultEntitySize * 0.99f));

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity colliderx = m_Manager.GetComponentData<Velocity>(entityx);
            Velocity collidery = m_Manager.GetComponentData<Velocity>(entityy);
            Velocity colliderz = m_Manager.GetComponentData<Velocity>(entityz);

            Assert.AreEqual(3, collider1.Collides);
            Assert.AreEqual(1, colliderx.Collides);
            Assert.AreEqual(1, collidery.Collides);
            Assert.AreEqual(1, colliderz.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_NegativeXYZEntities_BarelyTouch_Collide()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0));
            Entity entityx = CreateEntityWithDefaultData(new float3(DefaultEntitySize * -0.99f, 0, 0));
            Entity entityy = CreateEntityWithDefaultData(new float3(0, DefaultEntitySize * -0.99f, 0));
            Entity entityz = CreateEntityWithDefaultData(new float3(0, 0, DefaultEntitySize * -0.99f));

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity colliderx = m_Manager.GetComponentData<Velocity>(entityx);
            Velocity collidery = m_Manager.GetComponentData<Velocity>(entityy);
            Velocity colliderz = m_Manager.GetComponentData<Velocity>(entityz);

            Assert.AreEqual(3, collider1.Collides);
            Assert.AreEqual(1, colliderx.Collides);
            Assert.AreEqual(1, collidery.Collides);
            Assert.AreEqual(1, colliderz.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_NegativeXYZEntities_BarelyTouch_Extreme_Collide()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(-50000, 123456, -80000));
            Entity entityx = CreateEntityWithDefaultData(new float3(-50000 + DefaultEntitySize * -0.99f, 123456, -80000));
            Entity entityy = CreateEntityWithDefaultData(new float3(-50000, 123456 + DefaultEntitySize * -0.99f, -80000));
            Entity entityz = CreateEntityWithDefaultData(new float3(-50000, 123456, -80000 + DefaultEntitySize * -0.99f));

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity colliderx = m_Manager.GetComponentData<Velocity>(entityx);
            Velocity collidery = m_Manager.GetComponentData<Velocity>(entityy);
            Velocity colliderz = m_Manager.GetComponentData<Velocity>(entityz);

            Assert.AreEqual(3, collider1.Collides);
            Assert.AreEqual(1, colliderx.Collides);
            Assert.AreEqual(1, collidery.Collides);
            Assert.AreEqual(1, colliderz.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoEntities_JustOutOfRange_DontCollide()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0));
            Entity entity2 = CreateEntityWithDefaultData(new float3(DefaultEntitySize, 0, 0));

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(0, collider1.Collides);
            Assert.AreEqual(0, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_TwoEntities_JustOutOfRange_Extreme_DontCollide()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(99999, -45000, 3));
            Entity entity2 = CreateEntityWithDefaultData(new float3(99999 + 99999, -45000, -3));

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(0, collider1.Collides);
            Assert.AreEqual(0, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_ManyEntities_SamePositions_Collide()
        {
            int entityCount = 10;
            List<Entity> entities = new List<Entity>(entityCount);

            for (int i = 0; i < entityCount; i++)
            {
                Entity entity = CreateEntityWithDefaultData(new float3(0, 0, 0));
                entities.Add(entity);
            }

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            for (int i = 0; i < entities.Count; i++)
            {
                Velocity collider = m_Manager.GetComponentData<Velocity>(entities[i]);
                Assert.AreEqual(entityCount - 1, collider.Collides);
            }
        }

        [UnityTest]
        public IEnumerator Collision_ManyEntities_SamePositions_Extreme_Collide()
        {
            int entityCount = 10;
            List<Entity> entities = new List<Entity>(entityCount);

            for (int i = 0; i < entityCount; i++)
            {
                Entity entity = CreateEntityWithDefaultData(new float3(-95412, -40000, -1));
                entities.Add(entity);
            }

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            for (int i = 0; i < entities.Count; i++)
            {
                Velocity collider = m_Manager.GetComponentData<Velocity>(entities[i]);
                Assert.AreEqual(entityCount - 1, collider.Collides);
            }
        }

        [UnityTest]
        public IEnumerator Collision_TwoEntities_DifferentPositions_DontCollide()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, 0, 0));
            Entity entity2 = CreateEntityWithDefaultData(new float3(Consts.CellSize, 0, 0));

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreEqual(0, collider1.Collides);
            Assert.AreEqual(0, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator Collision_ManyEntities_DifferentPositions_DontCollide()
        {
            int entityCount = 10;
            List<Entity> entities = new List<Entity>(entityCount);

            for (int i = 0; i < entityCount; i++)
            {
                Entity entity = CreateEntityWithDefaultData(new float3(i, i, i) * Consts.CellSize);
                entities.Add(entity);
            }

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            for (int i = 0; i < entities.Count; i++)
            {
                Velocity collider = m_Manager.GetComponentData<Velocity>(entities[i]);
                Assert.AreEqual(0, collider.Collides);
            }
        }

        [UnityTest]
        public IEnumerator Collision_ManyEntities_DifferentPositions_Extreme_DontCollide()
        {
            int entityCount = 10;
            List<Entity> entities = new List<Entity>(entityCount);

            for (int i = 0; i < entityCount; i++)
            {
                Entity entity = CreateEntityWithDefaultData(new float3(i, i, i) * Consts.CellSize * 100000);
                entities.Add(entity);
            }

            UpdateWorld();
            yield return new WaitForEndOfFrame();

            for (int i = 0; i < entities.Count; i++)
            {
                Velocity collider = m_Manager.GetComponentData<Velocity>(entities[i]);
                Assert.AreEqual(0, collider.Collides);
            }
        }


        [UnityTest]
        public IEnumerator CellOverlap_TwoEntities_DifferentCells_DifferentHash()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(0, DefaultEntitySize, 0));
            Entity entity2 = CreateEntityWithDefaultData(new float3(0, -DefaultEntitySize, 0));

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            EntityCollider collider1 = m_Manager.GetComponentData<EntityCollider>(entity1);
            EntityCollider collider2 = m_Manager.GetComponentData<EntityCollider>(entity2);

            Assert.AreNotSame(collider1, collider2);
            Assert.AreNotEqual(collider1.CellHash, collider2.CellHash);
        }

        [UnityTest]
        public IEnumerator CellOverlap_TwoEntities_OneOverlapsCell_Collide()
        {
            float halfCellSize = Consts.CellSize * 0.5f;

            Entity entity1 = CreateEntityWithDefaultData(new float3(halfCellSize, halfCellSize, 0));
            Entity entity2 = CreateEntityWithDefaultData(new float3(halfCellSize, halfCellSize, -DefaultEntitySize * 0.8f));

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreNotSame(collider1, collider2);
            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator CellOverlap_TwoEntities_BothOverlapCell_Collide()
        {
            float halfCellSize = Consts.CellSize * 0.5f;

            Entity entity1 = CreateEntityWithDefaultData(new float3(halfCellSize, halfCellSize, DefaultEntitySize * 0.4f));
            Entity entity2 = CreateEntityWithDefaultData(new float3(halfCellSize, halfCellSize, -DefaultEntitySize * 0.4f));

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreNotSame(collider1, collider2);
            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator CellOverlap_TwoEntities_BothOverlapMultipleCells_Collide()
        {
            Entity entity1 = CreateEntityWithDefaultData(new float3(DefaultEntitySize * 0.1f, DefaultEntitySize * 0.1f, DefaultEntitySize * 0.1f));
            Entity entity2 = CreateEntityWithDefaultData(new float3(-DefaultEntitySize * 0.1f, -DefaultEntitySize * 0.1f, -DefaultEntitySize * 0.1f));

            UpdateWorld();

            yield return new WaitForEndOfFrame();

            Velocity collider1 = m_Manager.GetComponentData<Velocity>(entity1);
            Velocity collider2 = m_Manager.GetComponentData<Velocity>(entity2);

            Assert.AreNotSame(collider1, collider2);
            Assert.AreEqual(1, collider1.Collides);
            Assert.AreEqual(1, collider2.Collides);
        }

        [UnityTest]
        public IEnumerator CellOverlap_ManyEntities_AcrossManyCells_Collide()
        {
            const int entityCount = 50;
            List<Entity> entities = new List<Entity>(entityCount);

            for (int i = 0; i < entityCount; i++)
            {
                Entity entity = CreateEntityWithDefaultData(-10 + new float3((DefaultEntitySize - 0.01f) * i, 0, 0));
                entities.Add(entity);
            }

            UpdateWorld();

            yield return new WaitForEndOfFrame();


            Velocity collider0 = m_Manager.GetComponentData<Velocity>(entities[0]);
            Assert.AreEqual(1, collider0.Collides);
            for (int i = 1; i < entities.Count - 1; i++)
            {
                Velocity collider = m_Manager.GetComponentData<Velocity>(entities[i]);
                Assert.AreEqual(2, collider.Collides);
            }

            Velocity colliderLast = m_Manager.GetComponentData<Velocity>(entities[entities.Count-1]);
            Assert.AreEqual(1, colliderLast.Collides);
        }
    }
}