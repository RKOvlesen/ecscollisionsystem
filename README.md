# Custom Collision detection with ECS and Jobs

As there is currently no built-in collision system that works with Jobs and ECS, I am working on creating my own system.


A short video https://twitter.com/Vedilion/status/1028675414301073408


## Requirements

For a long time, I have been working on a space-sim, where I have set myself some odd requirements:
*  Huge world.
*  All units update in realtime
*  Multiplayer (~4-10 players)
*  Simple physics


To active this, I need a collision system that can handle a huge world and tons of entities, but does not need full rigidbody physics.


## The algoritm

//TODO


###

For updates see [My Twitter](https://twitter.com/Vedilion)
